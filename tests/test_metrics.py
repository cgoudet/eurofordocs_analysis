import polars as pl
import pytest
from polars import col
from polars.testing import assert_frame_equal

from src.metrics import gini_index, herfindahl_index


class TestGini:
    def test_gini_single_value(self):
        series = pl.Series([100])
        expected_gini = 0.0
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_equal_values(self):
        series = pl.Series([100, 100, 100])
        expected_gini = 0.0
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_varying_values(self):
        series = pl.Series([100, 200, 300, 400, 500])
        expected_gini = 0.2667
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_real_world_data(self):
        series = pl.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        expected_gini = 0.3
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_high_inequality(self):
        series = pl.Series([1, 1, 1, 1, 1000])
        expected_gini = 0.8  # High inequality scenario
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_zero_values(self):
        series = pl.Series([0, 0, 0, 0, 0])
        expected_gini = 0.0  # All zero values should have a Gini index of 0
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)

    def test_gini_mixed_values(self):
        series = pl.Series([1000, 200, 300, 500, 2000, 3000, 900, 5000, 7000, 10000])
        expected_gini = 0.55151
        assert gini_index(series) == pytest.approx(expected_gini, 0.01)


class TestHerfindal:
    def test_equality(self):
        inp = pl.DataFrame({"montant": [10, 10, 10, 10]})
        out = inp.select(herfindahl_index(col("montant")))
        exp = pl.DataFrame({"herfindahl_index": [0.0]})
        assert_frame_equal(out, exp)

    def test_high_inequality(self):
        inp = pl.DataFrame({"montant": [99, 1]})
        out = inp.select(herfindahl_index(col("montant")))
        exp = pl.DataFrame({"herfindahl_index": [0.9604]})
        assert_frame_equal(out, exp)
