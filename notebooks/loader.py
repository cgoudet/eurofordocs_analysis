%load_ext autoreload
%autoreload 2
%matplotlib inline
%load_ext dotenv
%dotenv

import numpy as np
import os
import polars as pl
import pandas as pd
import matplotlib as mpl
import sys
from pathlib import Path
from polars import col
from datetime import date, datetime
import matplotlib.pyplot as plt

# Make nicer matplotlib plots by default
mpl.rcParams["axes.spines.top"] = False
mpl.rcParams["axes.spines.right"] = False
mpl.rcParams["text.usetex"] = False

ROOTDIR = Path(".") / ".." / ".."
if str(ROOTDIR) not in sys.path:
    sys.path.insert(0, str(ROOTDIR))

COREDIR = ROOTDIR / '..'/'cgoudetcore'
if str(COREDIR) not in sys.path:
    sys.path.insert(0, str(COREDIR))

from src.duck import read_sql
