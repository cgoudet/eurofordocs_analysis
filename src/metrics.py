import polars as pl


def gini_index(series: pl.Series) -> float:
    n = len(series)
    if n == 0:
        return 1.0
    sorted_series = series.sort()
    cumulative_index = n + 1 - pl.Series(range(1, n + 1))
    numerator = (sorted_series * cumulative_index).sum()
    denominator = sorted_series.sum()
    if denominator == 0:
        return 0.0
    return (n + 1) / n - 2 * numerator / n / denominator


def herfindahl_index(montant: pl.Expr) -> pl.Expr:
    scale = 1 / montant.count().clip(lower_bound=1)
    raw_score = montant.pow(2).sum() / montant.sum().pow(2)
    return ((raw_score - scale) / (1 - scale)).alias("herfindahl_index")
