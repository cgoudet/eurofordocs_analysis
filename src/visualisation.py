import matplotlib.pyplot as plt
import numpy as np


def funnel_chart(labels, values, ax=None):
    if ax is None:
        _, ax = plt.subplots()

    values = [*list(values), values[-1]]
    n_labels = len(labels)
    for i in range(n_labels):
        xlim = np.array([values[i], values[i + 1]]) / 2
        ax.fill_betweenx(
            y=[n_labels - i, n_labels - i - 0.9],
            x1=-xlim,
            x2=xlim,
            color="cornflowerblue",
        )

    ax.set_xticks([], [])
    ax.set_yticks(np.arange(n_labels) + 0.4, labels[::-1])

    for y, value in zip(np.arange(n_labels) + 0.4, values[:-1][::-1], strict=False):
        ax.text(0, y, value, fontsize=10, color="white", fontweight="bold", ha="center")

    ax.set_ylabel("Stages")

    return ax
