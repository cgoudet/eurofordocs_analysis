from pathlib import Path

ROOTDIR = Path(__file__).parent / ".."
DATADIR = ROOTDIR / "data"
