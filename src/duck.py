import os

import duckdb
import polars as pl


def read_sql(rq: str) -> pl.DataFrame:
    with duckdb.connect(os.environ.get("DUCKDB_FP"), read_only=True) as con:
        return pl.read_database(rq, con)
