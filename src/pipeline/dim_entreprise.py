import polars as pl
import structlog
from polars import col

from src import ROOTDIR
from src.metrics import gini_index
from src.pipeline.common import declarations, entreprises
from src.pipeline.utils import add_segment_pcts

logger = structlog.get_logger()


def basic_aggregates(liens: pl.DataFrame) -> pl.DataFrame:
    return liens.group_by("entreprise_id").agg(
        col("montant").sum().alias("montant_total"),
        col("montant_masque").mean().alias("rate_masked"),
        col("efd_id").n_unique().alias("n_beneficiaires"),
        col("montant").map_elements(gini_index, return_dtype=pl.Float64).alias("gini"),
        col("efd_id").count().alias("n_declarations"),
    )


def dim_entreprise(declarations: pl.DataFrame) -> pl.DataFrame:
    dimension = (
        entreprises()
        .with_columns(entreprise_id=col("entreprise_id").cast(pl.Int32))
        .join(declarations.pipe(basic_aggregates), on="entreprise_id")
        .pipe(add_segment_pcts, declarations, user_id="entreprise_id", column="motif")
        .pipe(add_segment_pcts, declarations, user_id="entreprise_id", column="lien")
    )
    logger.info(
        "dim_entreprises", shape=dimension.shape, columns=sorted(dimension.columns)
    )
    return dimension


if __name__ == "__main__":
    declas = declarations()
    dimension = dim_entreprise(declas)
    dimension.write_parquet(ROOTDIR / "data" / "03_primary" / "dim_entreprise.parquet")
