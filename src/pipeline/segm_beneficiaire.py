import polars as pl
import structlog
from polars import col

from src import ROOTDIR
from src.pipeline.common import declarations

logger = structlog.get_logger()

OUTPUT_DIR = ROOTDIR / "data" / "04_feature"


def main():
    profession = "pharmacien"
    declas = declarations().filter(col("profession") == profession)
    logger.info(
        "segment_beneficiaire",
        profession=profession,
        n_benef=declas["efd_id"].n_unique(),
    )
    log_montant_motif(declas, profession)
    log_montant_motif_lien(declas, profession)
    count_montant_motif(declas, profession)
    count_montant_motif(declas, profession)
    count_montant_motif_lien(declas, profession)


def log_pivot(
    declarations: pl.DataFrame, columns: str | list[str], aggfunc: str = "sum"
):
    frame = (
        declarations.pivot(
            index="efd_id",
            columns=columns,
            values="montant",
            aggregate_function=aggfunc,
            sort_columns=True,
        )
        .fill_null(0)
        .sort("efd_id")
        .select("efd_id", pl.exclude("efd_id").clip(lower_bound=1).log10())
    )
    return frame


def log_montant_motif(declarations: pl.DataFrame, profession: str):
    frame = log_pivot(declarations, "motif")
    frame.write_parquet(OUTPUT_DIR / f"{profession}_log_montant_motif.parquet")


def log_montant_motif_lien(declarations: pl.DataFrame, profession: str):
    frame = log_pivot(declarations, ["motif", "lien"])
    frame.write_parquet(OUTPUT_DIR / f"{profession}_log_montant_motif_lien.parquet")


def count_montant_motif(declarations: pl.DataFrame, profession: str):
    frame = log_pivot(declarations, ["motif", "montant_cat"], aggfunc="len")
    frame.write_parquet(OUTPUT_DIR / f"{profession}_count_montant_motif.parquet")


def count_montant_motif_lien(declarations: pl.DataFrame, profession: str):
    frame = log_pivot(declarations, ["motif", "lien", "montant_cat"], aggfunc="len")
    frame.write_parquet(OUTPUT_DIR / f"{profession}_count_montant_motif_lien.parquet")


if __name__ == "__main__":
    main()
