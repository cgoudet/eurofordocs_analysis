import polars as pl
from polars import col


def add_segment_pcts(
    dimension: pl.DataFrame,
    liens: pl.DataFrame,
    user_id: str,
    column: str,
) -> pl.DataFrame:
    per_lien_benef = liens.pivot(
        index=user_id,
        columns=column,
        values="montant",
        aggregate_function="sum",
        sort_columns=True,
    ).fill_null(0)

    cols = [x for x in per_lien_benef.columns if x not in (user_id, "total")]
    renamed_cols = [f"pct_{column}_" + x.replace(" ", "_") for x in cols]

    per_lien_benef = per_lien_benef.rename(
        {x: r for x, r in zip(cols, renamed_cols, strict=False)}
    )

    merged = dimension.join(per_lien_benef, on=user_id, how="left").with_columns(
        *[
            (col(c) / col("montant_total").clip(lower_bound=1)).fill_null(0).round(4)
            for c in renamed_cols
        ]
    )
    return merged
