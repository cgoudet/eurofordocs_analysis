import polars as pl
import structlog
from cgoudetcore.ml.fastai import CollaborativeFiltering
from polars import col

from src import ROOTDIR
from src.pipeline.common import declarations

logger = structlog.get_logger()

OUTPUT_DIR = ROOTDIR / "data" / "04_feature"


def main():
    profession = "pharmacien"
    declas = declarations().filter(col("profession") == profession)
    logger.info(
        "segment_beneficiaire",
        profession=profession,
        n_benef=declas["efd_id"].n_unique(),
    )
    collaborative_filtering(declas, profession)


def collaborative_filtering(declarations: pl.DataFrame, profession: str):
    score = (
        declarations.group_by("efd_id", "entreprise_id")
        .agg(col("montant").sum())
        .with_columns(col("montant").clip(1).log10().alias("log_montant"))
    )
    collab = CollaborativeFiltering(
        random_state=42,
        batch_size=512,
        ndim=30,
        epochs=8,
        learning_rate=3e-3,
        item_name="entreprise_id",
        user_name="efd_id",
        rating_name="log_montant",
    )
    collab.fit(score.to_pandas())
    collab.user_emb.reset_index().to_parquet(
        OUTPUT_DIR / f"{profession}_collab_beneficiaires.parquet"
    )
    collab.item_emb.reset_index().to_parquet(
        OUTPUT_DIR / f"{profession}_collab_entreprises.parquet"
    )


if __name__ == "__main__":
    main()
