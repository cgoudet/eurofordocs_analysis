import polars as pl
import structlog
from polars import col

from src import ROOTDIR
from src.metrics import gini_index, herfindahl_index
from src.pipeline.common import beneficiaires, declarations
from src.pipeline.utils import add_segment_pcts

logger = structlog.get_logger()


def basic_aggregates(liens):
    return liens.group_by("efd_id").agg(
        col("montant").sum().alias("montant_total"),
        col("montant_masque").mean().alias("rate_masked"),
        pl.col("entreprise_id").n_unique().alias("n_companies"),
        pl.col("montant")
        .map_elements(gini_index, return_dtype=pl.Float64)
        .alias("gini"),
        herfindahl_index(col("montant")),
    )


def dim_beneficiaire(declas: pl.DataFrame) -> pl.DataFrame:
    dimension = (
        beneficiaires()
        .join(declas.pipe(basic_aggregates), on="efd_id", how="left")
        .pipe(add_segment_pcts, declas, user_id="efd_id", column="motif")
        .pipe(add_segment_pcts, declas, user_id="efd_id", column="lien")
        .pipe(add_pct_large_ent, declas)
        .with_columns(
            col("montant_total").clip(lower_bound=1).log10().alias("log_montant_total")
        )
        .pipe(add_company_concentrations, declas=declas)
    )
    logger.info(
        "dim_beneficiaire", shape=dimension.shape, columns=sorted(dimension.columns)
    )
    return dimension


def add_company_concentrations(
    benef: pl.DataFrame, declas: pl.DataFrame
) -> pl.DataFrame:
    company_benef = (
        declas.group_by("efd_id", "entreprise_id")
        .agg(col("montant").sum())
        .group_by("efd_id")
        .agg(
            pl.col("montant")
            .map_elements(gini_index, return_dtype=pl.Float64)
            .alias("gini_company"),
            herfindahl_index(col("montant")).alias("herfindahl_company"),
        )
    )
    return benef.join(company_benef, on="efd_id", how="left")


def add_pct_large_ent(benef: pl.DataFrame, declas: pl.DataFrame) -> pl.DataFrame:
    top_entreprises = (
        declas.group_by("entreprise_id")
        .agg(col("montant").sum())
        .sort("montant")
        .tail(45)
    )
    dependency = (
        declas.filter(col("entreprise_id").is_in(top_entreprises["entreprise_id"]))
        .group_by("efd_id")
        .agg(col("montant").sum().alias("pct_large_ent"))
    )
    return benef.join(dependency, on="efd_id", how="left").with_columns(
        pct_large_ent=col("pct_large_ent").fill_null(0)
        / col("montant_total").clip(lower_bound=1)
    )


if __name__ == "__main__":
    declas = declarations()
    benefs = dim_beneficiaire(declas)
    benefs.write_parquet(ROOTDIR / "data" / "03_primary" / "dim_beneficiaire.parquet")
