import polars as pl
import structlog
from polars import col

from src import DATADIR

logger = structlog.get_logger()


DECLA_TO_READ = [
    "ben_categorie",
    "ben_profession",
    "efd_id",
    "entreprise_ts_id",
    "montant_masque",
    "montant",
    "type_declaration",
]


DECLA_TEMP_READ = [
    "ben_origin_id",
    "meta_motif_lien_interet",
    "nombre_avantages_lies",
    "nombre_remunerations_liees",
]


def declarations():
    liens = (
        pl.read_parquet(
            DATADIR / "01_raw" / "ts_declaration.parquet",
            columns=DECLA_TO_READ + DECLA_TEMP_READ,
        )
        .with_columns(col("montant").cast(pl.Int64))
        .filter(
            (col("type_declaration") != "Convention")
            | (
                (col("type_declaration") == "Convention")
                & (
                    col("nombre_avantages_lies") + col("nombre_remunerations_liees")
                    == 0
                )
            )
        )
        .rename(
            {
                "type_declaration": "lien",
                "entreprise_ts_id": "entreprise_id",
                "ben_profession": "profession",
            }
        )
        .with_columns(
            montant_cat=((col("montant") + 1).log10() * 10).round(0).cast(pl.Int8),
            efd_id=col("ben_origin_id").fill_null(col("efd_id")),
            motif=col("meta_motif_lien_interet").fill_null(pl.lit("Autre")),
        )
        .drop(DECLA_TEMP_READ)
    )

    logger.info(
        "raw_liens",
        n_benef=liens["efd_id"].n_unique(),
        n_entreprise=liens["entreprise_id"].n_unique(),
        montant=int(liens["montant"].sum()),
    )
    return liens


def beneficiaires():
    return (
        pl.read_parquet(DATADIR / "01_raw" / "beneficiaire.parquet")
        .filter(col("origin_account").is_null())
        .drop("origin_account")
    )


def entreprises():
    return (
        pl.read_parquet(DATADIR / "01_raw" / "entreprise.parquet")
        .filter(col("mere_id").is_null())
        .drop("mere_id")
    )
