import click
import polars as pl
import structlog
from polars import col

from src import ROOTDIR
from src.pipeline.common import beneficiaires, declarations
from src.pipeline.dim_entreprise import dim_entreprise

logger = structlog.get_logger()


@click.command()
@click.option("--profession", type=str, help="Select a specific profession")
def csv_inputs(profession: str | None = None):
    declas = declarations()
    if profession:
        declas = declas.filter(col("profession") == profession)

    nodes = save_nodes(declas, profession)
    links = save_links(declas)
    check_coherence(nodes, links)


def save_nodes(
    declas: pl.DataFrame, profession: str | None
) -> tuple[pl.DataFrame, pl.DataFrame]:
    return (
        save_beneficiaires(profession),
        save_entreprises(declas),
    )


def save_beneficiaires(profession: str | None = None) -> pl.DataFrame:
    benefs = beneficiaires()
    if profession:
        benefs = benefs.filter(col("profession") == profession)
    benefs = (
        benefs.select(
            "efd_id",
            "categorie_code",
            "siren",
            "rpps",
            "adeli",
            "ordre",
            "rna",
            "nom",
            "prenom",
            "profession",
        )
        .with_columns(
            efd_id=pl.lit("b") + col("efd_id").cast(pl.String),
            categorie_code=pl.lit("Beneficiaire;") + col("categorie_code"),
        )
        .rename({"categorie_code": ":LABEL", "efd_id": "efd_id:ID"})
    )
    logger.info("node sizes", n_beneficiaire=benefs.shape[0])
    benefs.write_csv(
        ROOTDIR / "data" / "neo4j" / "import" / "knowledge_graph_beneficiaires.csv"
    )
    return benefs


def save_entreprises(declas: pl.DataFrame) -> pl.DataFrame:
    entreprises = (
        dim_entreprise(declas)
        .select("entreprise_id", "raison_sociale", "secteur_activite", "siren")
        .with_columns(
            (pl.lit("e") + col("entreprise_id").cast(pl.String)).alias("entreprise_id"),
            pl.lit("Emeteur;Entreprise").alias(":LABEL"),
        )
        .rename({"entreprise_id": "entreprise_id:ID"})
    )
    logger.info("node sizes", n_entreprises=entreprises.shape[0])
    entreprises.write_csv(
        ROOTDIR / "data" / "neo4j" / "import" / "knowledge_graph_emeteurs.csv"
    )
    return entreprises


def save_links(declas: pl.DataFrame) -> pl.DataFrame:
    declas = (
        declas.select("entreprise_id", "efd_id", "montant")
        .group_by("entreprise_id", "efd_id")
        .agg(col("montant").sum())
        .with_columns(
            (pl.lit("e") + col("entreprise_id").cast(pl.String)).alias("entreprise_id"),
            (pl.lit("b") + col("efd_id").cast(pl.String)).alias("efd_id"),
            pl.lit("DECLARE").alias(":TYPE"),
        )
        .rename(
            {
                "entreprise_id": ":START_ID",
                "efd_id": ":END_ID",
                "montant": "montant:int",
            }
        )
    )
    declas.write_csv(
        ROOTDIR / "data" / "neo4j" / "import" / "knowledge_graph_declarations.csv"
    )
    return declas


def check_coherence(nodes: tuple, links: pl.DataFrame):
    all_ids = set(links[":START_ID"]) | set(links[":END_ID"])
    for df in nodes:
        col = next(x for x in df.columns if ":ID" in x)
        all_ids = all_ids - set(df[col])

    if all_ids:
        logger.error(f"Missing nodes : {sorted(all_ids)[:5]}")


if __name__ == "__main__":
    csv_inputs()
